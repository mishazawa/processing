class Mover {
  PVector location;
  PVector velocity;
  PVector acceleration;

  float mass;

  Mover (float x, float y, float _mass) {
    location = new PVector(x, y);
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 0);
    mass = _mass;
  }


  void applyForce (PVector force) {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }

  PVector attract (Mover m) {
    PVector force = PVector.sub(location, m.location);
    float distance = force.mag();
    distance = constrain(distance, 5.0, 25.0);
    float strength = (9.81 * mass * m.mass) / (distance * distance);
    force.normalize();
    force.mult(strength);

    return force;
  }

  void bounce() {
    if (location.x > width || location.x < 0) {
      velocity.x *= -1;
    }

    if (location.y > height || location.y < 0) {
      velocity.y *= -1;
    }
  }

  void render () {
    bounce();

    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);

    stroke(0);
    strokeWeight(2);
    fill(127);
    ellipse(location.x, location.y, mass, mass);
  }
}
