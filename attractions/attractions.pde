Mover[] mvrs;

void setup () {
  size(400, 300);

  mvrs = new Mover[10];

  for (int i = 0; i < 10; i++) {
    mvrs[i] = new Mover((width/2) + 10 * i, height/2 - i, random(i, 10));
  }

}

void draw () {
  background(255);
  for (int i = 0; i < mvrs.length; i++) {
    for (int j = 0; j < mvrs.length; j++) {
      if (i != j) {
        PVector mattr = mvrs[i].attract(mvrs[j]);
        mvrs[j].applyForce(mattr);

        PVector nattr = mvrs[j].attract(mvrs[i]);
        mvrs[i].applyForce(nattr);
      }
    }
  }
  for (int i = 0; i < mvrs.length; i++) {
    mvrs[i].render();
  }
}
