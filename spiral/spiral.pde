PVector anchor;

float radius;
float angularSpeed = 0.01;
float angle = 0.01;

void setup() {
  size(400, 400);
  background(255);
  anchor = new PVector(width/2, height/2);

  radius = 0;
  angle = 0.01;
}

void draw() {

  float x = radius * cos(angle);
  float y = radius * sin(angle);

  translate(anchor.x, anchor.y);
  fill(0);
  ellipse(x, y, 5, 5);

  angle += 0.01;
  radius += 0.02;
}
