class Spaceship {
  PVector location;
  PVector velocity;
  PVector acceleration;

  float angle = 0.0;
  float aVelocity = 0.0;
  float aAcceleration = 0.0;


  Spaceship () {
    location = new PVector(width / 2, height / 2);
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 0);
  }

  void accelerate () {
    float x = 0.5 * cos(angle);
    float y = 0.5 * sin(angle);
    acceleration = new PVector(x, y);
  }

  void rotateDirection (float val) {
    aAcceleration = val;
  }

  void checkEdges () {
    if (location.x > width) {
      location.x = 0;
    } else if (location.x < 0) {
      location.x = width;
    }
    if (location.y > height) {
      location.y = 0;
    } else if (location.y < 0) {
      location.y = height;
    }
  }

  void update () {
    velocity.add(acceleration);
    velocity.limit(MAX_SPEED);
    location.add(velocity);

    aVelocity += aAcceleration;
    aVelocity = constrain(aVelocity, -0.1, 0.1);
    angle += aVelocity;

    acceleration.mult(0);
    aAcceleration = 0;
    aVelocity = 0;
  }

  void draw () {
    // draw fire
    if (acceleration.mag() != 0) {
      pushMatrix();
      noStroke();
      translate(location.x, location.y);
      rotate(angle);
      fill(255, 0, 0);
      ellipse(-30, 0, 15, 5);
      popMatrix();
    }

    update();
    checkEdges();

    stroke(0);
    strokeWeight(2);

    // draw ship
    pushMatrix();
    noFill();
    translate(location.x, location.y);
    rotate(angle);
    triangle(-20, 8, -20, -8, 0, 0);
    popMatrix();

    // draw engine
    pushMatrix();
    fill(127);
    translate(location.x, location.y);
    rotate(angle);
    rect(-20, 0, 5, 10);
    popMatrix();

  }

}
