Spaceship sp;

float MAX_SPEED = 10.0;

void setup () {
  size(400, 400);
  sp = new Spaceship();
}

void draw () {
  background(255);
  rectMode(CENTER);
  sp.draw();

}

void keyPressed () {
  if (key == 'a') sp.rotateDirection(-0.1);
  if (key == 'd') sp.rotateDirection(0.1);
  if (key == 'w') sp.accelerate();
}
