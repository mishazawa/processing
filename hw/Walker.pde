class Walker {
  int x, y;
  int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

  Walker () {
    x = width / 2;
    y = height / 2;
  }

  void draw () {
    stroke(0);
    point(x, y);
  }

  void step () {
    int[] dir = directions[int(random(4))];
    x += dir[0];
    y += dir[1];
    x = constrain(x, 0, width - 1);
    y = constrain(y, 0, height - 1);
  }

}
