class Particle {
  PVector location;
  PVector velocity;
  PVector acceleration;

  float life;
  float mass;

  Particle (PVector origin) {
    life = 255;
    mass = random(0.1, 2);
    location = new PVector(origin.x, origin.y);
    velocity = new PVector();
    acceleration = new PVector(random(-0.5, 0.5), random(-0.5, 0.5));
  }

  void applyForce (PVector force) {
    acceleration.add(force);
  }

  void update () {
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
    life -= 2;
  }

  void display () {
    noStroke();
    fill(random(200, 255), 0, 0, life);
    ellipse(location.x, location.y, 2* mass, 4* mass);
  }

  boolean isDead () {
    return life <= 0;
  }
}
