import java.util.*;

class ParticleSystem {
  ArrayList<Particle> particles;
  PVector origin;

  ParticleSystem (int originX, int originY) {
    origin = new PVector(originX, originY);
    particles = new ArrayList<Particle>();
  }

  PVector calcAttraction (Particle p1, Particle p2) {
    PVector force = PVector.sub(p1.location, p2.location);
    float distance = force.mag();
    distance = constrain(distance, 5.0, 25.0);
    float strength = (9.81 * p1.mass * p2.mass) / (distance * distance);
    force.normalize();
    force.mult(strength);
    return force;
  }

  void attract (Particle p1, Particle p2) {
    p1.applyForce(calcAttraction(p1, p2));
    p2.applyForce(calcAttraction(p2, p1));
  }

  void applyAtrraction () {
    for (Particle p1 : particles) {
      for (Particle p2 : particles) {
        attract(p1, p2);
      }
    }
  }

  void applyForce (PVector force) {
    for (Particle p : particles) {
      p.applyForce(force);
    }
  }

  void update () {
    particles.add(new Particle(origin));
  }

  void display () {
    Iterator<Particle> iter = particles.iterator();

    while (iter.hasNext()) {
      Particle p = iter.next();
      p.update();
      p.display();

      if (p.isDead()) {
        iter.remove();
      }

    }

  }

}
