class SquareParticle extends Particle {

  float angle = 0.0;

  SquareParticle (PVector origin) {
    super(origin);
  }

  void update () {
    super.update();
    angle += 0.1;
  }

  void display () {
    noStroke();
    fill(100, life);
    pushMatrix();
    translate(location.x, location.y);
    rotate(angle);
    rect(0, 0, 10, 10);
    popMatrix();
  }

}
