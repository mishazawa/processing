ParticleSystem ps;

void setup() {
  size(640, 360);
}

void draw() {
  background(255);

  if (ps != null) {
    PVector gravity = new PVector(0, 1);
    PVector wind = new PVector(0.1, 0);

    ps.applyForce(gravity);
    ps.applyForce(wind);
    ps.applyAtrraction();
    ps.display();
  }

}

void mouseClicked () {
  ps = new ParticleSystem(mouseX, mouseY);
  for (int i = 0; i < 10; i++) {
    ps.update();
  }
}
